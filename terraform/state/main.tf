variable "tag_type" {
  description = "The value for the tag named 'type'"
  type        = string
  default     = "xa-homework"
}

terraform {
  backend "s3" {
    bucket  = "xa-terraform-state"
    key     = "global/s3/terraform.tfstate"
    region  = "eu-west-1" # Ireland
    encrypt = true
  }
}

provider "aws" {
  region = "eu-west-1" # Ireland
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "xa-terraform-state"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    type = var.tag_type
  }
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.terraform_state.arn
  description = "The ARN of the S3 bucket"
}
