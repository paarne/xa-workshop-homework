# Terraform State

This Terraform code manages the S3 bucket that is used to store the Terraform state files (terraform.tfstate) in.

The bucket can be found from the [AWS S3 console](https://s3.console.aws.amazon.com/s3/home).
