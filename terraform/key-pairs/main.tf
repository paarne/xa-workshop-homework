variable "tag_type" {
  description = "The value for the tag named 'type'"
  type        = string
  default     = "xa-homework"
}

terraform {
  backend "s3" {
    bucket  = "xa-terraform-state"
    key     = "key-pairs/terraform.tfstate"
    region  = "eu-west-1" # Ireland
    encrypt = true
  }
}

provider "aws" {
  region = "eu-west-1" # Ireland
}

resource "aws_key_pair" "priit" {
  key_name   = "priit-key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJZ92lQmwB48Q4L6gf9YzI9EFmWJkdHBfQs1bleAydRt priit@Loom"

  tags = {
    type = var.tag_type
  }
}
