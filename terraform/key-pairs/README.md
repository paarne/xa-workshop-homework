# Key Pairs

This Terraform code manages the AWS key pairs.

The key pairs can be found from the [AWS EC2 Key Pairs](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#KeyPairs:) resource.
