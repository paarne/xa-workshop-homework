variable "tag_type" {
  description = "The value for the tag named 'type'"
  type        = string
  default     = "xa-homework"
}

terraform {
  backend "s3" {
    bucket  = "xa-terraform-state"
    key     = "security-groups/terraform.tfstate"
    region  = "eu-west-1" # Ireland
    encrypt = true
  }
}

provider "aws" {
  region = "eu-west-1" # Ireland
}

resource "aws_security_group" "web_server" {
  name        = "xa-web-server-security-group"
  description = "XA Workshop homework security group for the web server"

  ingress = [
    {
      description      = "Allows all inbound HTTP traffic"
      protocol         = "TCP"
      from_port        = 80
      to_port          = 80
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "Allows all inbound HTTPS traffic"
      protocol         = "TCP"
      from_port        = 443
      to_port          = 443
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "Allows inbound SSH traffic from my computer"
      protocol         = "TCP"
      from_port        = 22
      to_port          = 22
      cidr_blocks      = ["94.246.251.80/32"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress = [
    {
      description      = "Allows all outbound HTTP traffic"
      protocol         = "TCP"
      from_port        = 80
      to_port          = 80
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    },
    {
      description      = "Allows all outbound HTTPS traffic"
      protocol         = "TCP"
      from_port        = 443
      to_port          = 443
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  tags = {
    Name = "xa-web-server-security-group"
    type = var.tag_type
  }
}
