# Security Groups

This Terraform code manages the AWS security groups.

The security groups can be found from the [AWS EC2 Security Groups](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#SecurityGroups:) resource.
