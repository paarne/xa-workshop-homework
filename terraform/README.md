# Terraform

Holds all Terraform related infrastructure as code.

For a local development environment you need:
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
  - Make sure you have the correct [configuration](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)
  - GitLab's access keys are configured under Settings -> CI/CD -> Variables
- [Terraform](https://www.terraform.io/downloads.html)
