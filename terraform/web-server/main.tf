variable "tag_type" {
  description = "The value for the tag named 'type'"
  type        = string
  default     = "xa-homework"
}

terraform {
  backend "s3" {
    bucket  = "xa-terraform-state"
    key     = "web-server/terraform.tfstate"
    region  = "eu-west-1" # Ireland
    encrypt = true
  }
}

provider "aws" {
  region = "eu-west-1" # Ireland
}

resource "aws_instance" "web_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  count         = 1
  key_name      = "priit-key"

  vpc_security_group_ids = [data.aws_security_group.web_server.id]

  root_block_device {
    volume_size           = 8
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true

    tags = {
      Name = "xa-web-server-${count.index}-volume"
      type = var.tag_type
    }
  }

  tags = {
    Name = "xa-web-server-${count.index}"
    type = var.tag_type
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_security_group" "web_server" {
  name = "xa-web-server-security-group"
  tags = {
    type = var.tag_type
  }
}

output "ip" {
  value       = formatlist("%s: %s", aws_instance.web_server.*.tags.Name, aws_instance.web_server.*.public_ip)
  description = "The public IP of the web servers"
}

output "username" {
  value       = "ubuntu"
  description = "The default username used to connect with"
}

output "image" {
  value       = format("%s, %s", data.aws_ami.ubuntu.description, data.aws_ami.ubuntu.architecture)
  description = "Info about the image"
}
