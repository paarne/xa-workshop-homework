# Web Server

This Terraform code manages the EC2 instance that runs the web server.

The instance can be found from the [AWS EC2 Instances](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:) resource.
